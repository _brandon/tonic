/*
Navbar
 */
(function ($) { // Begin jQuery
    $(function () { // DOM ready
        // Navbar
        // -------------------------

        $('nav ul li a:not(:only-child)').on('click', function (e) {
            $(this).siblings('.nav-dropdown').toggle();

            $('.nav-dropdown').not($(this).siblings()).hide();
            e.stopPropagation();
            e.preventDefault();
        });

        $('html').on('click', function () {
            $('.nav-dropdown').hide();
        });

        $('#nav-toggle').on('click', function () {
            $('nav ul').slideToggle();
            this.classList.toggle('active');
        });
    }); // end DOM ready
})(jQuery); // end jQuery
