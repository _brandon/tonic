# Tonic

Simplistic CSS Framework; a [Creative Mess](https://creativemess.ca) family.

### Installation

Tonic requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies 

```sh
$ cd tonic
$ yarn install
```
### Resources

| Resource | URL |
| ------ | ------ |
| Docs | [creativemess.ca/tonic](https://creativemess.ca/tonic) |


### Development

Want to contribute? Great!

Tonic uses Yarn + Gulp for fast developing.

### Todos

 - Everything

License
----

MIT


**Free Software, Hell Yeah!**
